provider "google" {
  credentials = file("/var/lib/jenkins/sc.json")
  project     = "training-172012"
  region      = "us-central1"
}